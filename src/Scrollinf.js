const NAME = 'scrollinf';
const DATA_KEY = 'md.' + NAME;
const EVENT_KEY = `.${DATA_KEY}`;

const Default = {
    url: '',
    distance: 200
}

export default class Scrollinf {
    constructor(element, config = {}) {
        this._element = element;
        this._$element = $(element);
        this.config = $.extend({}, Default, config);

        this._page = 1;
        this._loading = false;
        this._scrollTop = 0;

        this._$window = $(window);
        this._$document = $(document);
    }

    dispose() {
        this._$window.off(EVENT_KEY);

        this._element = null;
        this._$element = null;
        this.config = null;

        this._page = null;
        this._loading = null;
        this._scrollTop = null;

        this._$window = null;
        this._$document = null;
    }

    init() {
        this._enable();
    }

    _enable() {
        this._$window.on('scroll' + EVENT_KEY, $.proxy(this._handle, this));

        this._handle();
    }

    _disable() {
        this._$window.off(EVENT_KEY);

        let $loader = this._$element.next();

        if ($loader[0]) {
            $loader[0].style.display = 'none';
        }
    }

    _load() {
        if (this._loading) {
            return;
        }

        this._loading = true;
        this._page++;

        $.get(this.config.url || window.location.href, { page: this._page }, (response = {}) => {
            if (response.data) {
                this._$element.append(response.data);
                this._loading = false;

                this._handle();
            }

            if (!response.next_page_url) {
                this._disable();
            }
        });
    }

    _handle() {
        let scrollTop = this._$document.scrollTop();

        if (scrollTop > this._scrollTop || this._$element.height() <= this._$window.height()) {
            let scrollHeight = this._$document.height();
            let height = this._$window.height();

            if (scrollTop + height >= scrollHeight - this.config.distance) {
                this._load();
            }
        }

        this._scrollTop = scrollTop;
    }
}
